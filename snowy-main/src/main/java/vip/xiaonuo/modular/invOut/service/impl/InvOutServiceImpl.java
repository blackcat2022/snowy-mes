/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.invOut.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.xiaonuo.core.consts.CommonConstant;
import vip.xiaonuo.core.enums.CommonStatusEnum;
import vip.xiaonuo.core.exception.ServiceException;
import vip.xiaonuo.core.factory.PageFactory;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.core.util.PoiUtil;
import vip.xiaonuo.modular.invIn.entity.InvIn;
import vip.xiaonuo.modular.invIn.param.InvInParam;
import vip.xiaonuo.modular.invOut.entity.InvOut;
import vip.xiaonuo.modular.invOut.enums.InvOutExceptionEnum;
import vip.xiaonuo.modular.invOut.mapper.InvOutMapper;
import vip.xiaonuo.modular.invOut.param.InvOutParam;
import vip.xiaonuo.modular.invOut.result.invOutResult;
import vip.xiaonuo.modular.invOut.service.InvOutService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.xiaonuo.modular.invdetail.entity.InvDetail;
import vip.xiaonuo.modular.invdetail.mapper.InvDetailMapper;
import vip.xiaonuo.modular.invdetail.param.InvDetailParam;
import vip.xiaonuo.modular.invdetail.service.InvDetailService;
import vip.xiaonuo.modular.stockbalance.entity.StockBalance;
import vip.xiaonuo.modular.stockbalance.param.StockBalanceParam;
import vip.xiaonuo.modular.stockbalance.service.StockBalanceService;
import vip.xiaonuo.util.AutoCode;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 出库表service接口实现类
 *
 * @author gjx
 * @date 2022-08-22 16:54:41
 */
@Service
public class InvOutServiceImpl extends ServiceImpl<InvOutMapper, InvOut> implements InvOutService {

    @Resource
    private InvDetailService invDetailService;

    @Resource
    private StockBalanceService stockBalanceService;
    @Override
    public PageResult<invOutResult> page(InvOutParam invOutParam) {
        QueryWrapper<invOutResult> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(invOutParam)) {

            queryWrapper.lambda().eq(InvOut::getCategory, -1);
            // 根据唯一编码 查询
            if (ObjectUtil.isNotEmpty(invOutParam.getCode())) {
                queryWrapper.like("a.code", invOutParam.getCode());
            }
            // 根据出库类型 查询
            if (ObjectUtil.isNotEmpty(invOutParam.getOutTypes())) {
                String[] outTypeList = invOutParam.getOutTypes().split(",");
                queryWrapper.lambda().in(InvOut::getOutType, outTypeList);
            }
            // 根据出库仓库 查询
            if (ObjectUtil.isNotEmpty(invOutParam.getWarHouId())) {
                queryWrapper.lambda().like(InvOut::getWarHouId, invOutParam.getWarHouId());
            }
            // 根据备注 查询
            if (ObjectUtil.isNotEmpty(invOutParam.getRemarks())) {
                queryWrapper.lambda().like(InvOut::getRemarks, invOutParam.getRemarks());
            }
            // 根据出库时间 查询
            if (ObjectUtil.isNotEmpty(invOutParam.getTime())) {

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date date = new Date();
                try {
                    date = sdf.parse(invOutParam.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                //得到日历
                Calendar calendar = Calendar.getInstance();
                //把当前时间赋给日历
                calendar.setTime(date);
                queryWrapper.lambda().ge(InvOut::getTime, sdf.format(calendar.getTime()));
                //设置为后一天
                calendar.add(Calendar.DAY_OF_MONTH, +1);
                queryWrapper.lambda().lt(InvOut::getTime, sdf.format(calendar.getTime()));
            }
        }
        //进行二次数据处理
        //取到查到的数据
        Page<invOutResult> page = baseMapper.page(PageFactory.defaultPage(), queryWrapper);
        /*
         * ===============================================处理数据的准备工作 start ======================================
         */
        //取到所需要的数据
        List<InvDetail> invDetailList = invDetailService.list();
        Map<Long, List<InvDetail>> invDetailGroupByInvId = invDetailList.stream().collect(Collectors.groupingBy(InvDetail::getInvId));
        /*
         * ===============================================处理数据的准备工作 end ======================================
         */
        /*
         * ===============================================给入库明细list赋值 start ======================================
         */
        page.getRecords().forEach(item -> {
                    Long id = item.getId();
                    if (ObjectUtil.isEmpty(id)) {
                        return;
                    }
                    List<InvDetail> invDetails = invDetailGroupByInvId.get(item.getId());
                    if (ObjectUtil.isEmpty(invDetails)) {
                        return;
                    }
                    item.setInvDetailList(invDetails);
                }
        );
        /*
         * ===============================================给入库明细list赋值 end ======================================
         */
        return new PageResult<>(page);
    }

    @Override
    public List<InvOut> list(InvOutParam invOutParam) {
        return this.list();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(InvOutParam invOutParam) {
        //添加  唯一编码校验、数据不为空校验
        checkParam(invOutParam, false);

        /*
         * =======================================入库逻辑 start =====================================
         */
        if (ObjectUtil.isEmpty(invOutParam.getCode())) {
            //表名称前加入一个空格 用区分入库还是出库   加了空格是入库
            invOutParam.setCode(AutoCode.getCodeByService("dw_inv", this.getClass(), 0));
        }
        InvOut invOut = new InvOut();
        BeanUtil.copyProperties(invOutParam, invOut);
        //-1：出库
        invOut.setCategory(-1);
        this.save(invOut);
        /*
         * =======================================入库逻辑 end =====================================
         */
        /*
         * =======================================库存余额逻辑 start =====================================
         */
        List<StockBalanceParam> stockBalanceParamList=new ArrayList<>();
        invOutParam.getInvDetailList().forEach(invDetail -> {
            invDetail.setWarHouId(invOutParam.getWarHouId());
            //出库相对于库存余额来说 是删除
            StockBalanceParam stockBalanceParam = new StockBalanceParam();
            stockBalanceParam.setWarHouId(invDetail.getWarHouId());
            stockBalanceParam.setProId(invDetail.getProId());
            stockBalanceParam.setStoNum(invDetail.getNum());
            stockBalanceParamList.add(stockBalanceParam);
        });
        /*
         * =======================================库存余额逻辑 end =====================================
         */

        /*
         * =======================================库存明细逻辑 start =====================================
         */
        invOutParam.getInvDetailList().forEach(invDetail -> {
            invDetail.setInvId(invOut.getId());
            invDetail.setWarHouId(invOut.getWarHouId());
        });
        invDetailService.saveBatch(invOutParam.getInvDetailList());
        deleteStockBalance(stockBalanceParamList);
        /*
         * =======================================库存明细逻辑 end =====================================
         */




    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<InvOutParam> invOutParamList) {
        List<StockBalanceParam> stockBalanceParamList=new ArrayList<>();
        invOutParamList.forEach(invOutParam -> {
            /*
             * =======================================删除库存明细 start =====================================
             */
            QueryWrapper<InvDetail> queryWrapper = new QueryWrapper<>();
            queryWrapper.lambda().eq(InvDetail::getInvId, invOutParam.getId());
            List<InvDetail> invDetailList = invDetailService.list(queryWrapper);
            invDetailList.forEach(invDetail -> {
                /*
                 * =======================================删除库存余额逻辑 start =====================================
                 */
                //修改根据仓库id和产品id修改仓库余额
                StockBalanceParam stockBalanceParam = new StockBalanceParam();
                stockBalanceParam.setWarHouId(invDetail.getWarHouId());
                stockBalanceParam.setProId(invDetail.getProId());
                stockBalanceParam.setStoNum(invDetail.getNum());
                stockBalanceParamList.add(stockBalanceParam);
                //addStockBalance(invDetail);
                /*
                 * =======================================库存余额逻辑 end =====================================
                 */
                invDetailService.removeById(invDetail.getId());
            });

            /*
             * =======================================删除库存明细 end =====================================
             */
            /*
             * =======================================删除入库 start =====================================
             */
            this.removeById(invOutParam.getId());
            /*
             * =======================================删除入库 end =====================================
             */
        });
        addStockBalance(stockBalanceParamList);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(InvOutParam invOutParam) {
        //添加  唯一编码校验
        checkParam(invOutParam, true);
        //如果编码为空，手动添加编码
        if (ObjectUtil.isEmpty(invOutParam.getCode())) {
            throw new ServiceException(8, "编号不能为空，请输入编号");
        }
        InvOut invOut=this.getById(invOutParam.getId());
        BeanUtil.copyProperties(invOutParam,invOut);
        this.updateById(invOut);
        /*
         * =======================================修改库存明细和库存余额 start =====================================
         */
        List<InvDetail> invDetailList = invOutParam.getInvDetailList();
        //取得数据库中的该出入库单的所有明细
        LambdaQueryWrapper<InvDetail> lqw = new LambdaQueryWrapper<>();
        lqw.eq(InvDetail::getInvId, invOutParam.getId());
        List<InvDetail> invDetails = invDetailService.list(lqw);
        Map<Long,InvDetail> invDetailMap=invDetails.stream().collect(Collectors.toMap(InvDetail::getId,a -> a, (k1, k2) -> k1));
        List<Long> invDetailIds = new ArrayList<>();
        invDetails.forEach(invDetail->{
            invDetailIds.add(invDetail.getId());
        });
        Long invInId = invOutParam.getId();
        List<StockBalanceParam> deletestockBalanceList =new ArrayList<>();
        List<InvDetail> editInvDetail=new ArrayList<>();
        List<InvDetail> addInvDetail=new ArrayList<>();
        List<Long> deleteInvDetail=new ArrayList<>();
        List<StockBalanceParam> addstockBalanceParamList=new ArrayList<>();
        for (InvDetail invDetail : invDetailList
        ) {
            invDetail.setInvId(invInId);
            //修改明细里面的仓库id
            invDetail.setWarHouId(invOutParam.getWarHouId());
            //没有明细id的进行添加
            if (ObjectUtil.isEmpty(invDetail.getId())) {
                // 添加库存明细
                addInvDetail.add(invDetail);
                // 减少库存余额
                invDetail.setWarHouId(invOutParam.getWarHouId());
                StockBalanceParam stockBalanceParam = new StockBalanceParam();
                stockBalanceParam.setWarHouId(invDetail.getWarHouId());
                stockBalanceParam.setProId(invDetail.getProId());
                stockBalanceParam.setStoNum(invDetail.getNum());
                deletestockBalanceList.add(stockBalanceParam);
            }
            //已有明细id的进行修改
            if (ObjectUtil.isNotEmpty(invDetail.getId())) {
                invDetailIds.remove(invDetail.getId());
                InvDetail oldInvDetail = invDetailMap.get(invDetail.getId());
                //增加现在的仓库余额
                invDetail.setWarHouId(invOutParam.getWarHouId());
                StockBalanceParam stockBalanceParam1 = new StockBalanceParam();
                stockBalanceParam1.setWarHouId(oldInvDetail.getWarHouId());
                stockBalanceParam1.setProId(oldInvDetail.getProId());
                stockBalanceParam1.setStoNum(oldInvDetail.getNum());
                addstockBalanceParamList.add(stockBalanceParam1);
                //删除原产品明细下的仓库余额
                StockBalanceParam stockBalanceParam = new StockBalanceParam();
                stockBalanceParam.setWarHouId(invDetail.getWarHouId());
                stockBalanceParam.setProId(invDetail.getProId());
                stockBalanceParam.setStoNum(invDetail.getNum());
                deletestockBalanceList.add(stockBalanceParam);
                editInvDetail.add(invDetail);
            }
        }
        //剩下的进行删除
        for (Long invDetailId : invDetailIds
        ) {
            InvDetail invDetail = invDetailMap.get(invDetailId);
            StockBalanceParam stockBalanceParam = new StockBalanceParam();
            stockBalanceParam.setWarHouId(invDetail.getWarHouId());
            stockBalanceParam.setProId(invDetail.getProId());
            stockBalanceParam.setStoNum(invDetail.getNum());
            addstockBalanceParamList.add(stockBalanceParam);
            deleteInvDetail.add(invDetailId);
        }
        addStockBalance(addstockBalanceParamList);
        deleteStockBalance(deletestockBalanceList);
        invDetailService.saveBatch(addInvDetail);
        invDetailService.updateBatchById(editInvDetail);
        invDetailService.removeByIds(deleteInvDetail);
        /*
         * =======================================修改库存明细和库存余额 end =====================================
         */


    }

    @Override
    public InvOut detail(InvOutParam invOutParam) {
        return this.queryInvOut(invOutParam);
    }

    /**
     * 获取出库表
     *
     * @author gjx
     * @date 2022-08-22 16:54:41
     */
    private InvOut queryInvOut(InvOutParam invOutParam) {
        InvOut invOut = this.getById(invOutParam.getId());
        if (ObjectUtil.isNull(invOut)) {
            throw new ServiceException(InvOutExceptionEnum.NOT_EXIST);
        }
        return invOut;
    }

    @Override
    public void export(InvOutParam invOutParam) {
        List<InvOut> list = this.list(invOutParam);
        PoiUtil.exportExcelWithStream("SnowyInvOut.xls", InvOut.class, list);
    }
    private void checkParam(InvOutParam invOutParam, boolean isExcludeSelf) {
        //校验 产品名称、变更数量是否为空
        if (ObjectUtil.isEmpty(invOutParam.getInvDetailList())) {
            throw new ServiceException(3, "产品不能为空，请添加产品");
        }
        if (ObjectUtil.isEmpty(invOutParam.getOutType())) {
            throw new ServiceException(6, "出库类型不能为空");
        }
        if (ObjectUtil.isEmpty(invOutParam.getTime())) {
            throw new ServiceException(7, "出库时间不能为空");
        }
        //添加产品的名称 、数量进项校验
        for (InvDetail invDetail : invOutParam.getInvDetailList()) {
            if (ObjectUtil.isEmpty(invDetail.getProId())) {
                throw new ServiceException(3, "产品不能为空，请添加产品");
            }
            if (ObjectUtil.isEmpty(invDetail.getNum()) || invDetail.getNum() <= 0) {
                throw new ServiceException(4, "出库数量不能为空并且必须大于零，请重新输入");
            }
        }
        LambdaQueryWrapper<InvOut> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        //如果是编辑需要去除自己
        if (isExcludeSelf) {
            lambdaQueryWrapper.ne(InvOut::getId, invOutParam.getId());
        }
        lambdaQueryWrapper.eq(InvOut::getCode, invOutParam.getCode());
        int count = this.count(lambdaQueryWrapper);
        if (count >= 1) {
            throw new ServiceException(2, "唯一编号已存在");
        }
    }

    //根据 明细删除库存余额
    private void deleteStockBalance(List<StockBalanceParam> stockBalanceParamList) {
        stockBalanceService.deleteStockBalance(stockBalanceParamList);

    }

    //根据 明细添加库存余额
    private void addStockBalance(List<StockBalanceParam> stockBalanceParams) {
        stockBalanceService.addStockBalance(stockBalanceParams);

    }

}
