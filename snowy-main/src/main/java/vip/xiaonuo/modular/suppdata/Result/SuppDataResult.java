package vip.xiaonuo.modular.suppdata.Result;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import vip.xiaonuo.modular.suppdata.entity.SuppData;

@Data
public class SuppDataResult extends SuppData {
    /**
     * 父类型名称
     */
    @Excel(name = "父类型名称")
    private String suppSortName;
}