/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.cusinfor.controller;

import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.cusinfor.param.CusInforParam;
import vip.xiaonuo.modular.cusinfor.service.CusInforService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 客户资料控制器
 *
 * @author czw
 * @date 2022-07-20 11:37:49
 */
@RestController
public class CusInforController {

    @Resource
    private CusInforService cusInforService;

    /**
     * 查询客户资料
     *
     * @author czw
     * @date 2022-07-20 11:37:49
     */
    @Permission
    @GetMapping("/cusInfor/page")
    @BusinessLog(title = "客户资料_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(CusInforParam cusInforParam) {
        return new SuccessResponseData(cusInforService.page(cusInforParam));
    }

    /**
     * 添加客户资料
     *
     * @author czw
     * @date 2022-07-20 11:37:49
     */
    @Permission
    @PostMapping("/cusInfor/add")
    @BusinessLog(title = "客户资料_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(CusInforParam.add.class) CusInforParam cusInforParam) {
            cusInforService.add(cusInforParam);
        return new SuccessResponseData();
    }

    /**
     * 删除客户资料，可批量删除
     *
     * @author czw
     * @date 2022-07-20 11:37:49
     */
    @Permission
    @PostMapping("/cusInfor/delete")
    @BusinessLog(title = "客户资料_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(CusInforParam.delete.class) List<CusInforParam> cusInforParamList) {
            cusInforService.delete(cusInforParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑客户资料
     *
     * @author czw
     * @date 2022-07-20 11:37:49
     */
    @Permission
    @PostMapping("/cusInfor/edit")
    @BusinessLog(title = "客户资料_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(CusInforParam.edit.class) CusInforParam cusInforParam) {
            cusInforService.edit(cusInforParam);
        return new SuccessResponseData();
    }

    /**
     * 查看客户资料
     *
     * @author czw
     * @date 2022-07-20 11:37:49
     */
    @Permission
    @GetMapping("/cusInfor/detail")
    @BusinessLog(title = "客户资料_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(CusInforParam.detail.class) CusInforParam cusInforParam) {
        return new SuccessResponseData(cusInforService.detail(cusInforParam));
    }

    /**
     * 客户资料列表
     *
     * @author czw
     * @date 2022-07-20 11:37:49
     */
    @Permission
    @GetMapping("/cusInfor/list")
    @BusinessLog(title = "客户资料_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(CusInforParam cusInforParam) {
        return new SuccessResponseData(cusInforService.list(cusInforParam));
    }

    /**
     * 导出系统用户
     *
     * @author czw
     * @date 2022-07-20 11:37:49
     */
    @Permission
    @GetMapping("/cusInfor/export")
    @BusinessLog(title = "客户资料_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(CusInforParam cusInforParam) {
        cusInforService.export(cusInforParam);
    }

}
