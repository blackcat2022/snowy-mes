import { axios } from '@/utils/request'

/**
 * 查询销售订单
 *
 * @author wyf
 * @date 2022-07-12 16:07:31
 */
export function saleOrderPage (parameter) {
  return axios({
    url: '/saleOrder/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 销售订单列表
 *
 * @author wyf
 * @date 2022-07-12 16:07:31
 */
export function saleOrderList (parameter) {
  return axios({
    url: '/saleOrder/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加销售订单
 *
 * @author wyf
 * @date 2022-07-12 16:07:31
 */
export function saleOrderAdd (parameter) {
  return axios({
    url: '/saleOrder/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑销售订单
 *
 * @author wyf
 * @date 2022-07-12 16:07:31
 */
export function saleOrderEdit (parameter) {
  return axios({
    url: '/saleOrder/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除销售订单
 *
 * @author wyf
 * @date 2022-07-12 16:07:31
 */
export function saleOrderDelete (parameter) {
  return axios({
    url: '/saleOrder/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出销售订单
 *
 * @author wyf
 * @date 2022-07-12 16:07:31
 */
export function saleOrderExport (parameter) {
  return axios({
    url: '/saleOrder/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
